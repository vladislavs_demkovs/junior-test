<?php
   // turn on/off error reporting
   error_reporting( E_ALL & ~E_DEPRECATED & ~E_NOTICE );

   // site url
   define('SITE_URL', 'http://localhost');

   // database related constants
   define('DB_DRIVER', 'mysql');                   // Database driver ...
   define('DB_SERVER', 'localhost');               // ... host name
   define('DB_SERVER_USERNAME', 'root');           // ... username
   define('DB_SERVER_PASSWORD', '');               // ... password
   define('DB_DATABASE', 'scandiweb_junior_test'); // ... name

   define('DS',DIRECTORY_SEPARATOR);

   define('__ROOT__', dirname(dirname(__FILE__)));

   define('APPLICATION_PATH', dirname($_SERVER['DOCUMENT_ROOT']).DS.'src');

   define('TEMPLATE_PATH','SOURCE_PATH'.DS.'application'.DS.'template');

   define('LOG_PATH',dirname('SOURCE_PATH'.DS.'logs'));




    



