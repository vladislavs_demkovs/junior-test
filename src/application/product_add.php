<?php
    include '../src/database/db/database.php';
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../public/assets/css/product_add.css">
    <title>Product Add</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <h2 class="h">Product Add</h2>
        <div>
            <form form="addForm" name="form-submit" action="#" onsubmit="return validateForm()" method="post">
                <button type="submit" class="button" name="submit-product">Save</button>
                <hr>
                <div class="row">
                    <div class="col-25">
                        <label for="sku">SKU:</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="sku" name="sku" placeholder="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-25">
                        <label for="name">Name:</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="name" name="name" placeholder="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-25">
                        <label for="price">Price:</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="price" name="price" placeholder="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-25">
                        <label for="type">Type:</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="type" name="type" placeholder="">
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>

<script type="text/javascript">
  function validateForm() {
    var sku = document.forms["form-submit"]["sku"].value;
    var name = document.forms["form-submit"]["name"].value;
    var price = document.forms["form-submit"]["price"].value;
    var type = document.forms["form-submit"]["type"].value;
    if (sku == null || sku == "", name == null || name == "", price == null || price == "", type == null || type == "") {
      alert("! Please fill all fiels for displaying product in Product List page");
      return false;
    }
  }
</script>