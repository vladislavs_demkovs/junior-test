<?php
   if(isset($_POST["submit-product"]))
   {

   $error   = '';
   $sku     = '';
   $name    = '';
   $price   = '';
   $type    = '';

   function clean_text($string)
   {
      $string = trim($string);
      $string = stripslashes($string);
      $string = htmlspecialchars($string);
      return $string;
   }
   
   $sku    = $_POST['sku'];
   $name   = $_POST['name'];
   $price  = $_POST['price'];
   $type   = $_POST['type'];

      if(empty($_POST["name"]))
      {
         $error .= '<p><label class="text-danger">!Vārds : field is empty</label></p>';
      }
      if(empty($_POST["sku"]))
      {
         $error .= '<p><label class="text-danger">!SKU : field is empty</label></p>';
      }
      if(empty($_POST["price"]))
      {
         $error .= '<p><label class="text-danger">!Price : field is empty</label></p>';
      }
      if(empty($_POST["type"]))
      {
         $error .= '<p><label class="text-danger">!Type : field is empty</label></p>';
      }
      if($error == '')
      {
         $sku    = $_POST['sku'];
         $name   = $_POST['name'];
         $price  = $_POST['price'];
         $type   = $_POST['type'];


         $pdoInsQuery = "INSERT INTO `products`(`sku`, `name`, `price`, `type`) VALUES (:sku, :name, :price, :type)";
         $pdoResult = $db_conn -> prepare($pdoInsQuery);
         $pdoExec = $pdoResult -> execute(array(":sku"=>$sku, ":name"=>$name, ":price"=>$price, ":type"=>$type));
      {
         $error = '<label class="text-success">Paldies par ziņu. Centīsimies atbildēt pēc iespējas ātrāk :)</label>';
      }
      else
      {
         $error = '<label class="text-danger">Radās kļūda :(</label>';
      }
   }
}