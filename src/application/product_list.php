<!DOCTYPE html>
<html>
<head>
	<title>Product List</title>
	<link rel="stylesheet" type="text/css" href="../public/assets/css/product_list.css">
   <!-- <script src="../public/assets/js/checkbox_delete.js"></script> -->
</head>
<body>
	<form method="post" action="">
		<div class="container">
			<h1 class="h1">Product list</h1>
			<div class="">
				<div class="mass-deleta-action">
					<p>Maass Delete Action: 
						<input type="checkbox" id="checkedAll">
						<input type="submit" name="submit" value="Submit" class="btn">
					</p>
				</div>
				<hr>
			</div>
			<div class="row">
				<?php
				include '../src/database/db/database.php';
				
				//DELETE QUERTY
				if(isset($_REQUEST['submit']))
				{
					$delete_id = $_REQUEST['delete_id'];
					$a = implode(",",$delete_id);

					$query = $db_conn->prepare("DELETE FROM `products` WHERE `id` IN($a)");
					$query->execute();
				}
				
				//SELECT QUERTY
				$statement = $db_conn->prepare("SELECT * FROM products");
				$statement->execute();

				while($row = $statement->fetch()){
				?>
				<tr>
					<td>
						<div class="product-card">
							<input type="checkbox" name="delete_id[]" class="checkitem" value="<?php echo $row['id'];?>">
							<div class="text p-SKU"><?php echo $row['sku'];?></div>
							<div class="text p-name"><?php echo $row['name'];?></div>
							<div class="text p-price"><?php echo $row['price'].' $';?></div>
							<div class="text p-type"><?php echo $row['type'];?></div>
						</div>
					</td>
				</tr>
				<?php } ?>
			</div>
		</div>
	</form>
</body>
</html>

<!-- jQuery Code for mass delete action -->
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src="bootstrap.min.js"></script>
<script>
	$("#checkedAll").change(function(){
		$(".checkitem").prop("checked", $(this).prop("checked"))
	})
	$(".checkitem").change(function() {
		if($(this).prop("checked")==false){
			$("checkedAll").prop("checked", true)
		}
	})
</script>