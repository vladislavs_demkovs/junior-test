<?php /*
namespace Component;
use Exception;
    
class Template {
    
        private $data;
        public function assign(array $data) {
            $this->data = $data;
        }
        public function render(string $file) : string
        {
            if(is_array($this->data)) extract($this->data);
            $filePath = str_replace('\\',DS,TEMPLATE_PATH.DS.$file);
            try {
                ob_start();
                require $filePath;
                return ob_get_clean();
            } catch (Exception $exception) {
                throw new Exception('Template is not valid',0, $exception);
            }
        }
}