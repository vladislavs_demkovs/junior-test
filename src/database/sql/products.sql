-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2020 at 09:02 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb_junior_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_latvian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `type`) VALUES
(1, 'JVC200234', 'Acme DISC', '100.01', 'Size: 700 MB'),
(2, 'JVC200567', 'Acme DISC', '100.02', 'Size: 700 MB'),
(3, 'JVC200890', 'Acme DISC', '100.03', 'Size: 700 MB'),
(4, 'GGWP007', 'War and Peace', '20.04', 'Weight: 2 KG'),
(5, 'GGWP008', 'War and Peace', '20.05', 'Weight: 2 KG'),
(6, 'GGWP009', 'War and Peace', '20.06', 'Weight: 2 KG'),
(7, 'GGWP099', 'War and Peace', '20.07', 'Weight: 2 KG'),
(8, 'TR120555', 'Chair', '40.08', 'Dimension: 24x45x15'),
(9, 'TR120556', 'Chair', '40.09', 'Dimension: 24x45x15'),
(10, 'TR120557', 'Chair', '40.10', 'Dimension: 24x45x15'),
(11, 'TR120558', 'Chair', '40.11', 'Dimension: 24x45x15'),
(12, 'TR120559', 'Chair', '40.12', 'Dimension: 24x45x15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
